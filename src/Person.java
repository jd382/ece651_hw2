public class Person {
	public String gender;
	public String lastName;
	public String firstName;
	public String nationality;
	public String identity;
	public String expr;
	public String degree;
	public String hobby;
	public String position;
	public String company;
	
	public Person(String gender, String lastName, String firstName, String identity, String nationality) {
		this.gender = gender;
		this.lastName = lastName;
		this.firstName = firstName;
		this.identity = identity;
		this.nationality = nationality;
	}
		
	public String whoIs(Person input) {
		String ans = input.firstName + " " + input.lastName + " is " + input.identity;
		if (gender == "male") {
			ans += " and he is from ";
	    }else {
			ans += " and she is from ";
	    }
		ans += input.nationality;
		return ans;
	}
}
