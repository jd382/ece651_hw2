import java.util.Hashtable;
import java.util.Scanner;

public class MainClass {
    
    @SuppressWarnings("resource")
    public static void insert(Hashtable<String, Person> names) {
    	System.out.println("Are you in Duke?(yes/no) ");
    	String duke = new Scanner(System.in).nextLine();
    	System.out.println("Last name: ");
    	String lN = new Scanner(System.in).nextLine();
    	System.out.println("First name: ");
    	String fN = new Scanner(System.in).nextLine();
    	System.out.println("Gender(male/female): ");
    	String gender = new Scanner(System.in).nextLine();
    	System.out.println("Nationality: ");
    	String nationality = new Scanner(System.in).nextLine();
		System.out.println("Identity: ");
		String identity = new Scanner(System.in).nextLine();
    	if ((duke.toLowerCase()).equals("yes")) {
    		System.out.println("Degree: ");
    		String degree = new Scanner(System.in).nextLine();
    		System.out.println("School: ");
    		String school = new Scanner(System.in).nextLine();
    		System.out.println("Hobby: ");
    		String hobby = new Scanner(System.in).nextLine();
    		System.out.println("Working expr(none if don't have): ");
    		String expr = new Scanner(System.in).nextLine();
    		if ((expr.toLowerCase()).equals("none")) {
    			BlueDevil p_0 = new BlueDevil(gender, lN, fN, nationality, identity, degree + " from " + school, hobby, null);
    			names.put(fN + " " + lN, p_0);
    		}else {
    			BlueDevil p_0 = new BlueDevil(gender, lN, fN, nationality, identity, degree + " from " + school, hobby, expr);
    			names.put(fN + " " + lN, p_0);
    		}
    	}else {
    		Person common = new Person(gender, lN, fN, identity, nationality);
    		names.put(fN + " " + lN, common);
    	}
    }
    
    @SuppressWarnings("resource")
	public static void main(String[] args) {
		Hashtable<String, Person> names = new Hashtable<String, Person> ();
    	names.put("Jianwei Du", new BlueDevil("male", "Du", "Jianwei", "China", "MS ECE student", "undergraduate from UMN", "animation", "ZTE Corporation"));
    	names.put("Shalin Shah", new BlueDevil("male", "Shalin", "Shalin", "India", "PhD ECE candidate","undergraduate from DA-IICT",  "bodybuilding and dancing", null));
    	names.put("Yuanyuan Yu", new BlueDevil("female", "Yu", "Yuanyuan", "China", "MEng ECE student","undergraduate from ECUST",  "baseball and fencing", null));
    	names.put("You Lyu", new BlueDevil("male", "Lyu", "You", "China", "MS ECE student","undergraduate from UNNC",  "traveling, music and history", null));
    	names.put("Zhongyu Li", new BlueDevil("male", "Li", "Zhongyu", "China", "MS ECE student","undergraduate from SEU",  "basketball & NBA", null));
    	names.put("Lei Chen", new BlueDevil("female", "Chen", "Lei", "China", "MS ECE student","undergraduate from KAIST",  "climbing and animals", null));
    	names.put("Adel Fahmy", new BlueDevil("male", "Fahmy", "Adel", "Egypt", "Adjunct Assistant Professor","MS in ECE from NCSU",  "tennis, biking, gardening and cooking", "IBM"));
    	names.put("Ric Telford", new BlueDevil("male", "Telford", "Ric", "U.S.", "Executive-in-Residence and Adjunct Assistant Professor","BS in CS from Trinity University",  "golf, sand volleyball, swimming and biking", "IBM"));
    	names.put("Ming Yao", new Person("male", "Yao", "Ming", "basketball player", "China"));
    	names.put("Donald Trump", new Person("male", "Trump", "Donald", "president", "U.S."));
    	while(true) {
    		System.out.print("Search or Insert(exit to close): ");
    		String choice = new Scanner(System.in).nextLine();
    		if ((choice.toLowerCase()).equals("search")) {
    			System.out.println("Last name: ");
    	    	String lN = new Scanner(System.in).nextLine();
    	    	System.out.println("First name: ");
    	    	String fN = new Scanner(System.in).nextLine();
    	    	Person p = names.get(fN + " " + lN);
    			if (p != null) {
    				System.out.println(p.whoIs(p));
    			}else {
    				System.out.println("Not found!");
    			}
    		}else if ((choice.toLowerCase()).equals("insert")) {
    			insert(names);
    		}else if ((choice.toLowerCase()).equals("exit")) {
    			break;
    		}else {
    			System.out.println("Invalid input!");
    		}
    	}
    }
}
