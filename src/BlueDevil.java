public class BlueDevil extends Person {
	BlueDevil(String gender, String lastName, String firstName, String nationality, String identity, String degree, String hobby, String expr) {
		super(gender, lastName, firstName, identity, nationality);
		this.gender = gender;
		this.lastName = lastName;
		this.firstName = firstName;
		this.nationality = nationality;
		this.identity = identity;
		this.expr = expr;
		this.degree = degree;
		this.hobby = hobby;	
	}
	
	public String whoIs(Person input) {
		String ans = input.firstName + " " + input.lastName + " is from " + input.nationality + " and is a " + input.identity + ". ";
		if (gender == "male") {
			ans += "He ";
	    }else {
			ans += "She ";
	    }
		if (expr == null) {
			ans += "does not have prior work experience. ";
		}else {
			ans += "worked in " + expr + " before. ";
		}
		if (gender == "male") {
			ans += "He received his ";
	    }else {
			ans += "She received her ";
	    }		
		ans += input.degree + ". When not in class, " + input.firstName + " enjoys " + input.hobby + ".";
		return ans;
	}
	
}